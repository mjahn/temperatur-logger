# WIFI Temperatur Sensort

Solarbetriebener WIFI Temperatur-Sensor mit Anbindung an Blynk und Thingspeak

## Benötigte Bibliotheken
Adafruit Unified Sensor
Adafruit DHT11 Sensor
Wifimanager
Blynk
