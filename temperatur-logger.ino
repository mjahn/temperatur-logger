#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <DHT.h>
#include <EEPROM.h>

#define DHTPOWERPIN 1          // pin where the DHT11 gets his power from
#define DHTPIN 2          // i7O pin where the DHT11 is connected
#define DHTTYPE DHT11     // What type of DHT11 is connected?

DHT dht(DHTPIN, DHTTYPE);
WiFiClient client;

// thingspeak API-key
char thingspeak_api_key[16];
char thingspeak_api_channel[16];
char thingspeak_api_host[256];

WiFiManagerParameter custom_thingspeak_api_host("thingspeak_api_host", "IOT Host", thingspeak_api_host, 256);
WiFiManagerParameter custom_thingspeak_api_key("thingspeak_api_key", "IOT API-Key", thingspeak_api_key, 16);
WiFiManagerParameter custom_thingspeak_api_channel("thingspeak_api_channel", "IOT Channel-Id", thingspeak_api_channel, 16);

// prepare the DHT-11 for measurements
void prepare()
{
  digitalWrite(DHTPOWERPIN, HIGH);
  dht.begin();
}

// get the temperature and humidity from 
void measure()
{

  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();

  if (isnan(humidity) || isnan(temperature)) {
    return;
  }

  publishData(temperature, humidity);
}

// save the custom configuration to EEPROM
void saveConfig()
{
  strcpy(thingspeak_api_host, custom_thingspeak_api_host.getValue());
  strcpy(thingspeak_api_key, custom_thingspeak_api_key.getValue());
  strcpy(thingspeak_api_channel, custom_thingspeak_api_channel.getValue());
  EEPROM.put(0, thingspeak_api_key);
  EEPROM.put(32, thingspeak_api_channel);
  EEPROM.put(64, thingspeak_api_host);
}

// read the custom configuration from EEPROM
void readConfig() 
{
  EEPROM.get(0, thingspeak_api_key);
  EEPROM.get(32, thingspeak_api_channel);
  EEPROM.get(64, thingspeak_api_host);
}


// publish data to service for further analysis
void publishData(float temperature, float humidity) 
{
  // Send data to ThingSpeak
  if (client.connect(thingspeak_api_host, 80)) {

    String postData = "";
    postData += "POST /data HTTP/1.1\r\n";
    postData += "Host: temperatur.logger\r\n";
    postData += "X-Authorization: ";
    postData += thingspeak_api_key;
    postData += "\r\n";
    postData += "Content-Type: application/json\r\n";
    postData += "{ \"channel\"=";
    postData += thingspeak_api_channel;
    postData += ", \"fields\": { \"1\": \"";
    postData += String(temperature);
    postData += "\", \"2\": ";
    postData += String(humidity);
    postData += "\" } }\r\n";
    postData += "Connection: close\r\n\r\n";
     
    client.print(postData);

    int timeout = millis() + 5000;
    while (client.available() == 0) {
      if (timeout - millis() < 0) {
        client.stop();
        return;
      }
    }
  }
}

// setup program - will be called once after reset
void setup()
{
  readConfig();
  
  WiFiManager wifiManager;
  wifiManager.addParameter(&custom_thingspeak_api_host);
  wifiManager.addParameter(&custom_thingspeak_api_key);
  wifiManager.addParameter(&custom_thingspeak_api_channel);
  wifiManager.setSaveConfigCallback(saveConfig);
  wifiManager.autoConnect("Wetterfrosch");

  prepare();
}

// will be called again and again
void loop()
{
  measure();
  ESP.deepSleep(1200 * 1000000);
}
